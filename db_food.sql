CREATE TABLE `food` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` float NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `food_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_type` (`type_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `food_type` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `like_res` (
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `date_like` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `like_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `like_res_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `like_res_ibfk_24` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `orders` (
  `user_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `arr_sub_id` varchar(100) DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `orders_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_ibfk_24` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `rate_res` (
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `date_rate` datetime NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rate_res_ibfk_24` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `restaurant` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `sub_food` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(100) NOT NULL,
  `sub_price` float NOT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(1, 'Hột vịt lộn xào me', 'img/pic1', 100, 'ngon', 1),
(2, 'Cá lóc nướng', 'img/pic2', 200, 'ngon', 2),
(3, 'Gà hấp bia', 'img/pic3', 300, 'ngon', 3),
(4, 'Tôm chiên bột', 'img/pic2', 250, 'ngon', 4),
(5, 'Vịt quay', 'img/pic4', 400, 'ngon', 5),
(6, 'Hào nướng phô mai', 'img/pic6', 150, 'ngon', 2),
(7, 'Ếch núp lùm', 'img/pic2', 120, 'ngon', 1),
(8, 'Cá lóc nướng', 'img/pic2', 200, 'ngon', 2),
(9, 'Hột vịt lộn xào me', 'img/pic1', 100, 'ngon', 1),
(10, 'Cá lóc nướng', 'img/pic2', 200, 'ngon', 2),
(11, 'Gà hấp bia', 'img/pic3', 300, 'ngon', 3),
(12, 'Tôm chiên bột', 'img/pic2', 250, 'ngon', 4),
(13, 'Vịt quay', 'img/pic4', 400, 'ngon', 5),
(14, 'Hào nướng phô mai', 'img/pic6', 150, 'ngon', 2),
(15, 'Ếch núp lùm', 'img/pic2', 120, 'ngon', 1),
(16, 'Cá lóc nướng', 'img/pic2', 200, 'ngon', 2)
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(1, 'xào'),
(2, 'nướng'),
(3, 'hấp'),
(4, 'chiên'),
(5, 'quay'),
(6, 'xào'),
(7, 'nướng'),
(8, 'hấp'),
(9, 'chiên'),
(10, 'quay')
INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`, `id`, `like_status`) VALUES
(1, 2, '2023-10-09 08:02:02', 38, 1);
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`, `id`) VALUES
(1, 1, 10, '01', 'abcd', 1),
(1, 2, 5, '01', 'abcd', 2),
(1, 3, 2, '01', 'abcd', 3),
(1, 4, 10, '01', 'abcd', 4),
(1, 5, 2, '01', 'abcd', 5),
(2, 1, 1, '01', 'abcd', 6),
(2, 2, 2, '01', 'abcd', 7),
(2, 3, 3, '01', 'abcd', 8),
(2, 4, 4, '01', 'abcd', 9),
(2, 5, 5, '01', 'abcd', 10)
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`, `id`) VALUES
(1, 1, 5, '2021-05-06 12:02:02', 1),
(2, 2, 4, '2021-05-06 12:02:02', 2),
(3, 1, 2, '2021-05-06 12:02:02', 3),
(4, 3, 5, '2021-05-06 12:02:02', 4),
(5, 2, 1, '2021-05-06 12:02:02', 5),
(6, 3, 5, '2021-05-06 12:02:02', 6),
(1, 1, 2, '2021-05-06 05:02:02', 7),
(1, 1, 2, '2023-10-07 17:00:00', 8)
INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
(1, 'SanFuLou', 'img/picture1', 'nhà hàng người hoa'),
(2, 'GogiHouse', 'img/picture2', 'nướng hàn quốc'),
(3, 'ThaiExpress', 'img/picture3', 'nhà hàng kiểu Thái'),
(4, 'SanFuLou', 'img/picture1', 'nhà hàng người hoa'),
(5, 'GogiHouse', 'img/picture2', 'nướng hàn quốc'),
(6, 'ThaiExpress', 'img/picture3', 'nhà hàng kiểu Thái');
INSERT INTO `user` (`user_id`, `full_name`, `email`, `password`) VALUES
(1, 'Trịnh Đình G', 'trinhdinhg@gmail.com', 'trinhdinhg'),
(2, 'Ngô Quang H', 'ngoquangh@gmail.com', 'ngoquangh'),
(3, 'Nguyễn Văn A', 'nguyenvanA@gmail.com', 'nguyenvana'),
(4, 'Trần Văn B', 'tranvanb@gmail.com', 'tranvanb'),
(5, 'Phạm Thị C', 'phamthic@gmail.com', 'phamthic'),
(6, 'Truơng Văn D', 'truongvand@gmail.com', 'truongvand'),
(7, 'Đỗ Văn E', 'dovane@gmail.com', 'dovane'),
(8, 'Hà Văn F', 'havanf@gmail.com', 'havanf'),
(9, 'Lê Văn G', 'levang@gmail.com', 'levang'),
(10, 'Trịnh Đình G', 'trinhdinhg@gmail.com', 'trinhdinhg'),
(11, 'Ngô Quang H', 'ngoquangh@gmail.com', 'ngoquangh'),
(12, 'Nguyễn Văn A', 'nguyenvanA@gmail.com', 'nguyenvana'),
(13, 'Trần Văn B', 'tranvanb@gmail.com', 'tranvanb'),
(14, 'Phạm Thị C', 'phamthic@gmail.com', 'phamthic'),
(15, 'Truơng Văn D', 'truongvand@gmail.com', 'truongvand'),
(16, 'Đỗ Văn E', 'dovane@gmail.com', 'dovane'),
(17, 'Hà Văn F', 'havanf@gmail.com', 'havanf'),
(18, 'Lê Văn G', 'levang@gmail.com', 'levang');
